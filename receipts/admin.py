from django.contrib import admin
from receipts.models import ExpenseCategory
from receipts.models import Account
from receipts.models import Receipt

# Register your models here
admin.site.register(Account)
admin.site.register(Receipt)
admin.site.register(ExpenseCategory)
