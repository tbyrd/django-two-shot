# from functools import total_ordering
# from socket import VM_SOCKETS_INVALID_VERSION
# from unicodedata import category
from django.shortcuts import render, redirect
from receipts.models import Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm
from receipts.models import ExpenseCategory
from receipts.forms import CategoryForm

# Create your views here.


@login_required
def receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_object": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_object": categories,
    }
    return render(
        request, "receipts/categories.html", context
    )  # Takes the data and the HTML and renders a page


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts_object": accounts,
    }
    return render(
        request, "receipts/accounts.html", context
    )  # Takes the data and the HTML and renders a page


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)