from django.urls import path
from accounts.views import user_login, user_logout
from accounts.views import signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
]
